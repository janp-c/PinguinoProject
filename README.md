#PinguinoProject

Todo lo que tenga que ver con la tarjeta Pinguino Project 
utilizando el PIC18F2550 y/o el PIC18F4550 (8 bits).

En mi blog "https://janp-c.blogspot.com/p/pinguino-project.html" encontrará 
un poco de información al respecto sobre la tarjeta Pinguino con algunos 
enlaces que le puede servir para desarrollar o fabricar su propia tarjeta 
o board de Pinguino (Open Software & Open Hardware).